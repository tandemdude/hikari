### Summary
<!-- Small summary of the feature -->

### Problem
<!-- What problem will this feature solve, if any -->

### Ideal implementation
<!-- How should this feature be implemented -->

### Checklist
<!-- Make sure to tick all the following boxes by putting an `x` in between (like this `[x]`) -->
- [ ] I have searched the issue tracker and have made sure its not a duplicate. If its a follow up of another issue, I have specified it.
